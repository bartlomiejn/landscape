# landscape

An OpenGL / GLFW based graphics engine.

Features:
- Texture-mapped materials
- Phong shading model 
- Directional, spot and point light sources
- Shadow mapping
- Noise generation

Tested on macOS 10.14.5.
